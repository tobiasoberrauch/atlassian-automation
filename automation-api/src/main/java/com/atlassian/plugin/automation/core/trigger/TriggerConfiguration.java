package com.atlassian.plugin.automation.core.trigger;

import com.atlassian.plugin.automation.core.AutomationConfiguration;

/**
 * Trigger configuration extending AutomationConfiguration
 */
public interface TriggerConfiguration extends AutomationConfiguration
{
}
