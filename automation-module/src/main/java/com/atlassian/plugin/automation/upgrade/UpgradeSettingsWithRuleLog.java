package com.atlassian.plugin.automation.upgrade;

import com.atlassian.plugin.automation.config.Settings;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.Collections;

/**
 * We added rule log, so this task just sets the defaults
 *
 * @since v5.0
 */
@Named
@ExportAsService
public class UpgradeSettingsWithRuleLog implements PluginUpgradeTask
{
    private static final Logger log = Logger.getLogger(UpgradeSettingsWithRuleLog.class);
    private final PluginSettings pluginSettings;

    @Inject
    public UpgradeSettingsWithRuleLog(@ComponentImport final PluginSettingsFactory pluginSettingsFactory)
    {
        pluginSettings = pluginSettingsFactory.createGlobalSettings();
    }

    @Override
    public int getBuildNumber()
    {
        return 4;
    }

    @Override
    public String getShortDescription()
    {
        return "Upgrades settings with default values for RuleLog.";
    }

    @Override
    public Collection<Message> doUpgrade() throws Exception
    {
        pluginSettings.put(Settings.PRODUCT_RULE_LOG_ENABLED, String.valueOf(false));
        return Collections.emptyList();
    }

    @Override
    public String getPluginKey()
    {
        return "com.atlassian.plugin.automation.automation-module";
    }
}