package com.atlassian.plugin.automation.scheduler;

import com.atlassian.plugin.automation.config.RuleConfigStore;
import com.atlassian.plugin.automation.core.CronTrigger;
import com.atlassian.plugin.automation.core.EventTrigger;
import com.atlassian.plugin.automation.core.Rule;
import com.atlassian.plugin.automation.core.Trigger;
import com.atlassian.plugin.automation.core.auditlog.EventAuditMessageBuilder;
import com.atlassian.plugin.automation.core.trigger.TriggerConfiguration;
import com.atlassian.plugin.automation.module.AutomationModuleManager;
import com.atlassian.plugin.automation.auditlog.EventAuditLogService;
import com.atlassian.plugin.automation.spi.registrar.RuleModificationListener;
import com.atlassian.plugin.automation.spi.registrar.RuleRegistrar;
import com.atlassian.plugin.automation.spi.scheduler.CronScheduler;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.google.common.collect.Maps;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Named
@SuppressWarnings("unused")
public class DefaultAutomationScheduler implements AutomationScheduler, InitializingBean, DisposableBean, RuleModificationListener
{
    private static final Logger log = Logger.getLogger(DefaultAutomationScheduler.class);

    private final TransactionTemplate transactionTemplate;
    private final AutomationModuleManager automationModuleManager;
    private final RuleExecutionLimiter ruleExecutionLimiter;
    private final EventAuditLogService eventAuditLogService;
    private final RuleRegistrar ruleRegistrar;
    private final ThreadLocalExecutor threadLocalExecutor;

    private final ThreadPoolExecutor eventExecutor;
    private final RuleConfigStore ruleStore;
    private final SettingsAwareRuleLogService ruleLogService;

    private final PluginSettingsFactory pluginSettingsFactory;

    @Inject
    public DefaultAutomationScheduler(
            @ComponentImport final RuleRegistrar ruleRegistrar,
            @ComponentImport final TransactionTemplate transactionTemplate,
            final ThreadLocalExecutor threadLocalExecutor,
            final EventAuditLogService eventAuditLogService,
            final AutomationModuleManager automationModuleManager,
            final RuleExecutionLimiter ruleExecutionLimiter,
            final RuleConfigStore ruleStore,
            final SettingsAwareRuleLogService ruleLogService,
            final PluginSettingsFactory pluginSettingsFactory)
    {
        this.ruleRegistrar = ruleRegistrar;
        this.threadLocalExecutor = threadLocalExecutor;
        this.eventAuditLogService = eventAuditLogService;
        this.transactionTemplate = transactionTemplate;
        this.automationModuleManager = automationModuleManager;
        this.ruleExecutionLimiter = ruleExecutionLimiter;
        this.eventExecutor = createThreadPoolExecutor();
        this.ruleStore = ruleStore;
        this.ruleLogService = ruleLogService;
        this.pluginSettingsFactory = pluginSettingsFactory;
    }

    @Override
    public synchronized void scheduleRule(final Rule rule)
    {
        scheduleRuleInternal(rule, true);
    }

    @Override
    public synchronized void unscheduleRule(int ruleId)
    {
        unscheduleRuleInternal(ruleId, true);
    }

    @Override
    public void unscheduleAllRules()
    {
        ruleRegistrar.unregisterAllRules();
    }

    @Override
    public void destroy() throws Exception
    {
        ruleRegistrar.unregisterRuleChangedHandler(this);
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        ruleRegistrar.registerRuleChangedHandler(this);
    }

    @Override
    public void onRuleModified(int ruleId)
    {
        final Rule rule = ruleStore.getRule(ruleId);

        // rule might got deleted, so depending if we can find it, re-schedule or unschedule it
        // Important!!! Don't notify on this change, as we are already handling this as part of the changed notification
        // If we send notification, clustered nodes will again trigger change notification, causing a loop
        if (rule != null)
        {
            scheduleRuleInternal(rule, false);
        }
        else
        {
            unscheduleRuleInternal(ruleId, false);
        }
    }

    private void scheduleRuleInternal(Rule rule, boolean notify)
    {
        if (ruleRegistrar.isRuleRegistered(rule.getId()))
        {
            log.debug("Trying to schedule already scheduled rule. Unscheduling rule: " + rule.getName());
            unscheduleRuleInternal(rule.getId(), notify);
        }

        if (!rule.isEnabled())
        {
            log.debug("Trying to schedule disabled rule. Skipping rule: " + rule.getName());
            return;
        }

        final TriggerConfiguration config = rule.getTriggerConfiguration();

        final Trigger trigger = automationModuleManager.getTrigger(config.getModuleKey());
        if (trigger != null)
        {
            trigger.init(config);
            final RuleCallableContext ruleContext = new RuleCallableContext(rule, threadLocalExecutor, eventAuditLogService, transactionTemplate,
                    automationModuleManager, ruleExecutionLimiter, ruleStore, this, ruleLogService);

            if (trigger instanceof EventTrigger)
            {
                final String eventClassName = ((EventTrigger) trigger).getEventClassName();
                final AutomationEventHandler eventHandler = new DefaultAutomationEventHandler(ruleContext, eventExecutor, eventClassName, pluginSettingsFactory);
                ruleRegistrar.registerEventTriggeredRule(rule.getId(), eventHandler, notify);
            }
            else if (trigger instanceof CronTrigger)
            {
                final Map<String, Object> params = Maps.newHashMap();
                params.put(CronScheduler.JOB_KEY_RULE_CONTEXT, ruleContext);
                params.put(CronScheduler.JOB_KEY_CRON_EXPRESSION, ((CronTrigger) trigger).getCronString());
                ruleRegistrar.registerCronTriggeredRule(rule.getId(), params, CronScheduledTriggerJob.class, notify);
            }
            else
            {
                throw new IllegalStateException("Invalid Trigger type configured for rule '" + rule.getName() + "'");
            }
        }
        else
        {
            if (notify)
            {
                eventAuditLogService.addEntry(new EventAuditMessageBuilder().setActor(rule.getActor()).
                        setRuleId(rule.getId()).
                        setMessage("Invalid configuration").
                        setErrors("Trigger module not present: " + rule.getTriggerConfiguration().getModuleKey()).build());
            }
        }
    }

    private void unscheduleRuleInternal(int ruleId, boolean notify)
    {
        ruleRegistrar.unregisterRule(ruleId, notify);
    }

    private static ThreadPoolExecutor createThreadPoolExecutor()
    {
        // This is essentially the same as Executors.newSingleThreadExecutor(), but has the advantage of
        // returning a ThreadPoolExecutor so that we can get thread pool statistics for debugging purposes
        return new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
    }
}
