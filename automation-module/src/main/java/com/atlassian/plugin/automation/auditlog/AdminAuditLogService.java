package com.atlassian.plugin.automation.auditlog;

/**
 * Provides access to Automation admin audit log
 */
public interface AdminAuditLogService extends AuditLogService
{

    /**
     * Adds admin action entry (ie. add, edit, delete rule, etc.)
     * @param adminAuditLogType type of admin audit log
     * @param user    authentication user
     * @param ruleId  id of the affected rule
     */
    void addAdminEntry(AdminAuditLogType adminAuditLogType, String user, int ruleId);

}
