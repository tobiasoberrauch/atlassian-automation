package com.atlassian.plugin.automation.admin.soy;

import com.atlassian.soy.renderer.SoyServerFunction;
import com.google.common.collect.ImmutableSet;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.TimeZone;

/**
 * Formats the date for server side soy templates (which didn't work correctly in JIRA5)
 */
public class DateFormatFunction implements SoyServerFunction<String>
{
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss");

    @Override
    public String getName()
    {
        return "automationFormatDate";
    }

    @Override
    public String apply(Object... args)
    {
        dateFormat.setTimeZone((TimeZone)args[1]);
        return dateFormat.format((Date) args[0]);
    }

    @Override
    public Set<Integer> validArgSizes()
    {
        return ImmutableSet.of(2);
    }
}