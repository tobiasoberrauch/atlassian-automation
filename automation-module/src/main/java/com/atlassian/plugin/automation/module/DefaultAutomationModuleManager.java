package com.atlassian.plugin.automation.module;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.automation.core.Action;
import com.atlassian.plugin.automation.core.Trigger;
import com.atlassian.plugin.automation.spi.build.BuildNumberService;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.tracker.DefaultPluginModuleTracker;
import com.atlassian.plugin.tracker.PluginModuleTracker;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.apache.log4j.Logger;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;

@Named
public class DefaultAutomationModuleManager implements AutomationModuleManager
{
    private static final Logger log = Logger.getLogger(DefaultAutomationModuleManager.class);

    private final PluginModuleTracker<Trigger, AutomationTriggerModuleDescriptor> triggerTracker;
    private final PluginModuleTracker<Action, AutomationActionModuleDescriptor> actionTracker;

    @Inject
    public DefaultAutomationModuleManager(
            @ComponentImport final PluginAccessor pluginAccessor,
            @ComponentImport final PluginEventManager pluginEventManager,
            @ComponentImport final BuildNumberService buildNumberService)
    {
        final BuildNumberCustomizer buildNumberCustomizer = new BuildNumberCustomizer(buildNumberService.getBuildNumber());
        triggerTracker = new DefaultPluginModuleTracker(pluginAccessor, pluginEventManager, AutomationTriggerModuleDescriptor.class, buildNumberCustomizer);
        actionTracker = new DefaultPluginModuleTracker(pluginAccessor, pluginEventManager, AutomationActionModuleDescriptor.class, buildNumberCustomizer);
    }

    @Override
    public Trigger getTrigger(final String key)
    {
        final AutomationTriggerModuleDescriptor triggerModuleDescriptor = Iterables.find(triggerTracker.getModuleDescriptors(),
                new Predicate<AutomationTriggerModuleDescriptor>()
                {
                    @Override
                    public boolean apply(@Nullable AutomationTriggerModuleDescriptor triggerModuleDescriptor)
                    {
                        return triggerModuleDescriptor != null && triggerModuleDescriptor.getCompleteKey().equals(key);
                    }
                }, null);
        if (triggerModuleDescriptor != null)
        {
            return triggerModuleDescriptor.getModule();
        }
        return null;
    }

    @Override
    public Action getAction(final String key)
    {
        final AutomationActionModuleDescriptor triggerModuleDescriptor = Iterables.find(actionTracker.getModuleDescriptors(),
                new Predicate<AutomationActionModuleDescriptor>()
                {
                    @Override
                    public boolean apply(@Nullable AutomationActionModuleDescriptor actionModuleDescriptor)
                    {
                        return actionModuleDescriptor != null && actionModuleDescriptor.getCompleteKey().equals(key);
                    }
                }, null);
        if (triggerModuleDescriptor != null)
        {
            return triggerModuleDescriptor.getModule();
        }
        return null;
    }

    @Override
    public Iterable<AutomationTriggerModuleDescriptor> getTriggerDescriptors()
    {
        return triggerTracker.getModuleDescriptors();
    }

    @Override
    public Iterable<AutomationActionModuleDescriptor> getActionDescriptors()
    {
        return actionTracker.getModuleDescriptors();
    }

    private static class BuildNumberCustomizer implements PluginModuleTracker.Customizer
    {
        private long applicationBuildNumber;

        private BuildNumberCustomizer(long applicationBuildNumber)
        {
            this.applicationBuildNumber = applicationBuildNumber;
        }

        @Override
        public ModuleDescriptor adding(ModuleDescriptor descriptor)
        {
            if (descriptor.getParams().containsKey("supportedFrom"))
            {
                final String supportedFromStr = String.valueOf(descriptor.getParams().get("supportedFrom"));

                try
                {
                    final long supportedFrom = Long.parseLong(supportedFromStr);
                    if (supportedFrom > applicationBuildNumber)
                    {
                        return null;
                    }
                }
                catch (NumberFormatException e)
                {
                    log.warn("Unable to parse version: " + supportedFromStr);
                }
            }
            return descriptor;
        }

        @Override
        public void removed(ModuleDescriptor descriptor)
        {

        }
    }
}
