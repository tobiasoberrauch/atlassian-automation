package com.atlassian.plugin.automation.module;

import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory;
import com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory;
import com.atlassian.plugin.spring.scanner.annotation.export.ModuleType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@ModuleType(ListableModuleDescriptorFactory.class)
@Component
public class AutomationTriggerModuleDescriptorFactory
        extends SingleModuleDescriptorFactory<AutomationTriggerModuleDescriptor>
{
    @Autowired
    public AutomationTriggerModuleDescriptorFactory(final HostContainer hostContainer)
    {
        super(hostContainer, "automation-trigger", AutomationTriggerModuleDescriptor.class);
    }
}
