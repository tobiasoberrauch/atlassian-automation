package com.atlassian.plugin.automation.config;

import com.atlassian.activeobjects.tx.Transactional;
import com.atlassian.plugin.automation.core.Rule;

/**
 * Handles Persistance of Rules
 */
@Transactional
public interface RuleConfigStore
{
    /**
     * Returns all currently stored rules.
     *
     * @return all currently stored rules.
     */
    Iterable<Rule> getRules();

    /**
     * Returns a specific rule
     *
     * @param ruleId id of the rule
     * @return rule or null
     */
    Rule getRule(int ruleId);

    /**
     * Return specific rule based on name
     *
     * @param name the name of the rule
     * @return rule or null
     */
    Rule getRule(String name);

    /**
     * Stores the provided rule in the persistence layer
     *
     * @param rule new rule to add
     * @return the newly stored rule
     */
    Rule addRule(Rule rule);

    /**
     * Delete the rule with the provided id and return the deleted rule
     *
     * @param ruleId id of the rule to delete
     * @return The deleted rule object.
     */
    Rule delete(int ruleId);

    /**
     * Change status of the rule with the provided id and return the changed rule
     *
     * @param ruleId id of the rule to delete
     * @param status the new rule status
     * @return The updated rule object.
     */
    Rule updateRuleStatus(int ruleId, boolean status);

    /**
     * Updates the rule
     * @param rule
     * @return
     */
    Rule updateRule(Rule rule);
}
