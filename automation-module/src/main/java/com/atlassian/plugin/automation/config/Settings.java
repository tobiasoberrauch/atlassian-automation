package com.atlassian.plugin.automation.config;

/**
 * This class contains setting keys used to access via PluginSettings
 */
public final class Settings
{
    /**
     * Enables/disables rules rate limiter
     */
    public static final String LIMITER_ENABLED = "automation.limiter.enabled";

    /**
     * Enables/disables product specific audit log
     */
    public static final String PRODUCT_RULE_LOG_ENABLED = "automation.product.rulelog.enabled";

    /**
     * Enables/disables synchronous event handling
     */
    public static final String SYNC_EVENT_HANDLING_ENABLED = "automation.synchronous.execution.enabled";

    private Settings()
    {
    }
}
