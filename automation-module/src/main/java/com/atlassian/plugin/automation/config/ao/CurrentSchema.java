package com.atlassian.plugin.automation.config.ao;

import net.java.ao.Entity;
import net.java.ao.OneToMany;
import net.java.ao.OneToOne;
import net.java.ao.Preload;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Table;
import net.java.ao.schema.Unique;

import java.util.Date;

/**
 * This is the current or "master" schema definitions that are referenced by application code.
 * <p/>
 * Whenever you have to make changes or add an AO upgrade task you need to do a few tasks :
 * <p/>
 * <pre>
 *      - Copy this file into previous VnnnSchema class
 *
 *              This allows us to track the changes to the schema over time
 * </pre>
 */
@SuppressWarnings ("UnusedDeclaration")
public class CurrentSchema
{
    @Table ("RULE_ENTITY")
    @Preload
    public static interface RuleEntity extends Entity
    {
        @NotNull
        @Unique
        String getRuleName();

        void setRuleName(String name);

        @OneToOne
        TriggerEntity getTrigger();

        @OneToMany
        ActionEntity[] getActions();

        @NotNull
        String getActor();

        void setActor(String actor);

        boolean getEnabled();

        void setEnabled(boolean status);
    }

    @Table ("TRIGGER_ENTITY")
    @Preload
    public static interface TriggerEntity extends Entity
    {
        RuleEntity getRuleEntity();

        @OneToMany
        TriggerConfigEntity[] getTriggerConfiguration();

        @StringLength (StringLength.UNLIMITED)
        String getCompleteModuleKey();
    }


    @Table ("ACTION_ENTITY")
    @Preload
    public static interface ActionEntity extends Entity
    {
        RuleEntity getRuleEntity();

        @OneToMany
        ActionConfigEntity[] getActionConfiguration();

        @StringLength (StringLength.UNLIMITED)
        String getCompleteModuleKey();
    }

    @Table ("TRIGGER_CONF_ENT")
    @Preload
    public static interface TriggerConfigEntity extends Entity
    {
        public static final String TRIGGER_CONFIG_ID = "TRIGGER_CONFIG_ENTITY_ID";

        @StringLength (StringLength.UNLIMITED)
        String getParamKey();

        TriggerEntity getTriggerEntity();

        @OneToMany
        TriggerConfigValueEntity[] getTriggerConfigValues();

        /**
         * @deprecated This has now been deprecated in favour of config values below. This method will eventually be removed
         */
        @StringLength (StringLength.UNLIMITED)
        @Deprecated
        String getParamValue();

        /**
         * @deprecated This has now been deprecated in favour of config values below. This method will eventually be removed
         */
        @StringLength (StringLength.UNLIMITED)
        @Deprecated
        void setParamValue(final String paramValue);
    }

    @Table ("ACTION_CONF_ENT")
    @Preload
    public static interface ActionConfigEntity extends Entity
    {
        public static final String ACTION_CONFIG_ID = "ACTION_CONFIG_ENTITY_ID";

        @StringLength (StringLength.UNLIMITED)
        String getParamKey();

        ActionEntity getActionEntity();

        @OneToMany
        ActionConfigValueEntity[] getActionConfigValues();

        /**
         * @deprecated This has now been deprecated in favour of config values below. This method will eventually be
         *             removed
         */
        @StringLength (StringLength.UNLIMITED)
        @Deprecated
        String getParamValue();

        /**
         * @deprecated This has now been deprecated in favour of config values below. This method will eventually be
         *             removed
         */
        @StringLength (StringLength.UNLIMITED)
        @Deprecated
        void setParamValue(final String paramValue);
    }

    @Table ("ACTION_CONFIG_VALUE")
    @Preload
    public static interface ActionConfigValueEntity extends Entity
    {
        ActionConfigEntity getActionConfigEntity();

        @StringLength (StringLength.UNLIMITED)
        String getParamValue();
    }

    @Table ("TRIGGER_CONFIG_VALUE")
    @Preload
    public static interface TriggerConfigValueEntity extends Entity
    {
        TriggerConfigEntity getTriggerConfigEntity();

        @StringLength (StringLength.UNLIMITED)
        String getParamValue();
    }


    /**
     * EVENT AUDIT LOG
     */

    @Table ("AUDIT_MESSAGE_ENTITY")
    @Preload
    public static interface AuditMessageEntity extends Entity
    {
        Date getDate();

        String getActor();

        int getRuleId();

        @StringLength (StringLength.UNLIMITED)
        String getMessage();

        @StringLength (StringLength.UNLIMITED)
        String getTriggerMessage();

        @OneToMany
        ActionMessage[] getActionMessages();

        @StringLength (StringLength.UNLIMITED)
        String getErrors();
    }

    @Table ("ACTION_MESSAGE")
    @Preload
    public static interface ActionMessage extends Entity
    {
        @StringLength (StringLength.UNLIMITED)
        String getMessage();

        AuditMessageEntity getAuditMessageEntity();
    }

    /**
     * ADMIN AUDIT LOG
     */
    @Table("ADMIN_AUDIT_MESSAGE")
    @Preload
    public static interface AdminAuditMessageEntity extends Entity
    {
        Date getDate();

        String getActor();

        int getRuleId();

        String getType();

        @StringLength(StringLength.UNLIMITED)
        String getMessage();
    }
}
