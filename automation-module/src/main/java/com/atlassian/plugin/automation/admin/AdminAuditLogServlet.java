package com.atlassian.plugin.automation.admin;

import com.atlassian.plugin.automation.auditlog.AdminAuditLogService;
import com.atlassian.plugin.automation.auditlog.AuditLogService;
import com.atlassian.plugin.automation.config.RuleConfigStore;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.timezone.TimeZoneManager;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.soy.renderer.SoyTemplateRenderer;

import javax.inject.Inject;

@Scanned
public class AdminAuditLogServlet extends AbstractAuditLogServlet
{
    private final AdminAuditLogService adminAuditLogService;

    @Inject
    public AdminAuditLogServlet(
            @ComponentImport final WebSudoManager webSudoManager,
            @ComponentImport final SoyTemplateRenderer renderer,
            @ComponentImport final UserManager userManager,
            @ComponentImport final LoginUriProvider loginUriProvider,
            @ComponentImport final WebResourceManager webResourceManager,
            @ComponentImport final TimeZoneManager timeZoneManager,
            final AdminAuditLogService adminAuditLogService,
            final RuleConfigStore ruleStore)
    {
        super(webSudoManager, renderer, userManager, loginUriProvider, webResourceManager, timeZoneManager, ruleStore);
        this.adminAuditLogService = adminAuditLogService;
    }

    @Override
    public AuditLogService getAuditLogService()
    {
        return adminAuditLogService;
    }

    @Override
    public String getAuditLogTemplateName()
    {
        return "Atlassian.Templates.Automation.showAdminAuditLog";
    }


}