AJS.namespace("Atlassian.Automation.RuleModel");
AJS.namespace("Atlassian.Automation.RuleCollection");

Atlassian.Automation.RuleModel = Brace.Model.extend({
    namedAttributes: ["name", "actor", "enabled", "status", "trigger", "actions", "triggerView", "actionViews"],
    urlRoot: AJS.contextPath() + "/rest/automation/1.0/rule",
    defaults: {
        name: "",
        actor: "",
        trigger: new Atlassian.Automation.TriggerModel(),
        actions: new Atlassian.Automation.ActionCollection()
    },

    initialize: function () {
    },

    parse: function (resp) {
        return {
            id: resp.rule.id,
            name: resp.rule.name,
            actor: resp.rule.actor,
            enabled: resp.rule.enabled,
            status: resp.rule.status,
            trigger: new Atlassian.Automation.TriggerModel(resp.rule.trigger, {parse: true}),
            actions: new Atlassian.Automation.ActionCollection(resp.rule.actions, {parse: true}),
            triggerView: resp.triggerView,
            actionViews: resp.actionViews
        };
    }
});

Atlassian.Automation.RuleCollection = Brace.Collection.extend({
    url: contextPath + "/rest/automation/1.0/rule",
    model: Atlassian.Automation.RuleModel
});