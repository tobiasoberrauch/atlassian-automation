package com.atlassian.plugin.automation.jira.action;

import com.atlassian.jira.bc.issue.properties.IssuePropertyService;
import com.atlassian.jira.entity.property.EntityPropertyService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.plugin.automation.core.Action;
import com.atlassian.plugin.automation.core.action.ActionConfiguration;
import com.atlassian.plugin.automation.core.auditlog.AuditString;
import com.atlassian.plugin.automation.core.auditlog.DefaultAuditString;
import com.atlassian.plugin.automation.jira.util.Constants;
import com.atlassian.plugin.automation.jira.util.ErrorCollectionUtil;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.automation.util.ParameterUtil;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.base.Joiner;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

import static com.atlassian.plugin.automation.util.ParameterUtil.singleValue;

@Scanned
public class SetIssuePropertyAction implements Action<Issue>
{
    public static final String SET_PROPERTY_FIELD_KEY = "setPropertyKeyField";
    public static final String SET_PROPERTY_VALUE_KEY = "setPropertyValueField";

    private static final Logger log = Logger.getLogger(SetIssuePropertyAction.class);

    private String issuePropertyKey;
    private String issuePropertyValue;
    private final UserManager userManager;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final IssuePropertyService issuePropertyService;

    @Inject
    public SetIssuePropertyAction(
            @ComponentImport final UserManager userManager,
            @ComponentImport final SoyTemplateRenderer soyTemplateRenderer,
            @ComponentImport final IssuePropertyService issuePropertyService,
            @ComponentImport final BuildUtilsInfo buildUtilsInfo)
    {
        this.userManager = userManager;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.issuePropertyService = issuePropertyService;
    }

    @Override
    public void init(ActionConfiguration config)
    {
        issuePropertyKey = singleValue(config, SET_PROPERTY_FIELD_KEY);
        issuePropertyValue = singleValue(config, SET_PROPERTY_VALUE_KEY);
    }

    @Override
    public void execute(String actor, Iterable<Issue> items, ErrorCollection errorCollection)
    {
        final ApplicationUser actorUser = userManager.getUserByName(actor);
        for (Issue issue : items)
        {
            final com.atlassian.jira.util.ErrorCollection jiraErrorCollection = new SimpleErrorCollection();

            final EntityPropertyService.PropertyInput propertyInput = new EntityPropertyService.PropertyInput(issuePropertyValue, issuePropertyKey);
            final EntityPropertyService.SetPropertyValidationResult setPropertyValidationResult = issuePropertyService.validateSetProperty(actorUser, issue.getId(), propertyInput);
            if (setPropertyValidationResult.isValid())
            {
                final EntityPropertyService.PropertyResult propertyResult = issuePropertyService.setProperty(actorUser, setPropertyValidationResult);
                if (!propertyResult.isValid())
                {
                    jiraErrorCollection.addErrorCollection(propertyResult.getErrorCollection());
                }
            }
            else
            {
                jiraErrorCollection.addErrorCollection(setPropertyValidationResult.getErrorCollection());
            }
            if (jiraErrorCollection.hasAnyErrors())
            {
                errorCollection.addErrorCollection(ErrorCollectionUtil.transform(jiraErrorCollection));
            }
        }
    }

    @Override
    public AuditString getAuditLog()
    {
        return new DefaultAuditString(String.format("Property '%s' set to '%s'", issuePropertyKey, issuePropertyValue));
    }

    @Override
    public String getConfigurationTemplate(ActionConfiguration actionConfiguration, String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, actionConfiguration);

            return soyTemplateRenderer.render(Constants.CONFIG_COMPLETE_KEY, "Atlassian.Templates.Automation.JIRA.setPropertyAction", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public String getViewTemplate(final ActionConfiguration actionConfiguration, final String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, actionConfiguration);

            return soyTemplateRenderer.render(Constants.CONFIG_COMPLETE_KEY, "Atlassian.Templates.Automation.JIRA.setPropertyActionView", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public ErrorCollection validateAddConfiguration(I18nResolver i18n, Map<String, List<String>> params, String actor)
    {
        final ErrorCollection errors = new ErrorCollection();
        if (!params.containsKey(SET_PROPERTY_FIELD_KEY) || StringUtils.isBlank(singleValue(params, SET_PROPERTY_FIELD_KEY)))
        {
            errors.addError(SET_PROPERTY_FIELD_KEY, i18n.getText("automation.action.setPropertyAction.field.empty"));
        }

        if (!params.containsKey(SET_PROPERTY_VALUE_KEY) || StringUtils.isBlank(singleValue(params, SET_PROPERTY_VALUE_KEY)))
        {
            errors.addError(SET_PROPERTY_VALUE_KEY, i18n.getText("automation.action.setPropertyAction.field.empty"));
        }

        // the validation only makes sense if there are no other errors
        if (!errors.hasAnyErrors())
        {
            final com.atlassian.jira.util.ErrorCollection validateErrorCollection = issuePropertyService.validatePropertyInput(
                    new EntityPropertyService.PropertyInput(singleValue(params, SET_PROPERTY_VALUE_KEY), singleValue(params, SET_PROPERTY_FIELD_KEY)));
            if (validateErrorCollection.hasAnyErrors())
            {
                final String errorMessagesTxt = Joiner.on(", ").join(validateErrorCollection.getErrorMessages());
                errors.addError(SET_PROPERTY_FIELD_KEY, errorMessagesTxt);
                errors.addError(SET_PROPERTY_VALUE_KEY, errorMessagesTxt);
            }
        }
        return errors;
    }
}
