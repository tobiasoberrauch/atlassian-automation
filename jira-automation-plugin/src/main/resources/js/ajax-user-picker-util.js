(function () {
    AJS.namespace("AJS.automation.AjaxUserPickerUtils");

    AJS.automation.AjaxUserPickerUtils.singleSelect = function(fieldID, context) {
        userSelect(fieldID, context, false);
    };

    AJS.automation.AjaxUserPickerUtils.multiSelect = function(fieldID, context) {
        userSelect(fieldID, context, true);
    };

    var userSelect = function(fieldID, context, isMultiSelect) {
        var options = {
            element: AJS.$("#" + fieldID, context),
            itemAttrDisplayed: "label",
            showDropdownButton: false,
            errorMessage: "The user \'\'{0}\'\' is not valid, it will be ignored",
            ajaxOptions: {
                url: function () {
                    return AJS.contextPath() + "/rest/api/1.0/users/picker"
                },
                data: {
                    showAvatar: true
                },
                query: true, // keep going back to the sever for each keystroke
                minQueryLength: 1,
                formatResponse: function (response) {
                    var ret = [];

                    AJS.$(response).each(function (i, suggestions) {
                        var groupDescriptor = new AJS.GroupDescriptor({
                            weight: i,
                            label: suggestions.footer
                        });
                        AJS.$(suggestions.users).each(function () {
                            groupDescriptor.addItem(new AJS.ItemDescriptor({
                                value: this.name,
                                label: this.displayName,
                                html: this.html,
                                icon: this.avatarUrl,
                                allowDuplicate: false,
                                highlighted: true
                            }))
                        });
                        ret.push(groupDescriptor)
                    });

                    return ret;
                }
            }
        }

        if(isMultiSelect) {
            new AJS.MultiSelect(options);
        } else {
            new AJS.SingleSelect(options);
        }

    };
})();
