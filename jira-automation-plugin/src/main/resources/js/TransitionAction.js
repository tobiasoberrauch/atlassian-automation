(function () {
    var init = function(e, $context) {
        new AJS.SingleSelect({
            element: AJS.$("#jiraActionId", $context),
            itemAttrDisplayed: "label",
            showDropdownButton:true,
            width:250
        });
    };

    AJS.$(AJS).bind("AJS.automation.form.loaded", init);
})();