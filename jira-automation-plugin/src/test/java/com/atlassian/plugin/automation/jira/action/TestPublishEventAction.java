package com.atlassian.plugin.automation.jira.action;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventTypeManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.automation.config.DefaultAutomationConfiguration;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TestPublishEventAction
{
    @Mock
    private UserManager userManager;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private SoyTemplateRenderer soyTemplateRenderer;
    @Mock
    private I18nResolver i18nResolver;
    @Mock
    private EventTypeManager eventTypeManager;

    @Before
    public void setUp() throws Exception
    {
        when(i18nResolver.getText(Matchers.anyString())).thenReturn("translated text");
    }

    @Test
    public void testExecutePasses() throws Exception
    {
        final PublishEventAction publishEventAction = new PublishEventAction(soyTemplateRenderer, userManager, eventPublisher, eventTypeManager);
        Map<String, List<String>> params = Maps.newHashMap();
        final Long eventId = 1l;
        params.put(PublishEventAction.EVENT_ID_KEY, Lists.newArrayList(String.valueOf(eventId)));
        publishEventAction.init(new DefaultAutomationConfiguration(-1, "", params));

        Issue issue = mock(MutableIssue.class);
        when(issue.getId()).thenReturn(1000l);
        List<Issue> issues = Lists.newArrayList(issue);
        ErrorCollection errorCollection = new ErrorCollection();

        ApplicationUser applicationUser = mock(ApplicationUser.class);
        when(userManager.getUserByName(Matchers.anyString())).thenReturn(applicationUser);

        final IssueEvent[] publishedEvent = new IssueEvent[1];
        doAnswer(new Answer()
        {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable
            {
                publishedEvent[0] = (IssueEvent) (invocation.getArguments()[0]);
                return null;
            }
        }).when(eventPublisher).publish(any());
        publishEventAction.execute("actor", issues, errorCollection);

        verify(eventPublisher).publish(Matchers.any(IssueEvent.class));
        assertFalse("errors present", errorCollection.hasAnyErrors());
        assertEquals("event id doesn't match", eventId, publishedEvent[0].getEventTypeId());
        assertSame("issue doesn't match", issue, publishedEvent[0].getIssue());

    }
}
