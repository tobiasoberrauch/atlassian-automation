package com.atlassian.plugin.automation.jira.trigger;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.UserCFType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.automation.config.DefaultAutomationConfiguration;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class TestIssueEventUserPredicate
{
    @Mock
    private CustomFieldManager customFieldManager;

    @Mock
    private Issue issue;

    private IssueEvent fredEvent;
    private IssueEvent adminEvent;

    @Mock
    private ApplicationUser admin;
    @Mock
    private ApplicationUser fred;

    @Mock
    private ApplicationUser applicationAdmin;
    @Mock
    private ApplicationUser applicationFred;

    @Mock
    private GroupManager groupManager;

    @Before
    public void setup()
    {
        when(admin.getName()).thenReturn("admin");
        when(admin.getKey()).thenReturn("admin");
        when(fred.getName()).thenReturn("fred");
        when(fred.getKey()).thenReturn("fred");

        when(applicationAdmin.getName()).thenReturn("admin");
        when(applicationAdmin.getKey()).thenReturn("admin");
        when(applicationFred.getKey()).thenReturn("fred");
        when(applicationFred.getName()).thenReturn("fred");

        when(groupManager.isUserInGroup("fred", "admins")).thenReturn(false);
        when(groupManager.isUserInGroup("admin", "admins")).thenReturn(true);

        fredEvent = new IssueEvent(issue, Maps.newHashMap(), fred, 1L);
        adminEvent = new IssueEvent(issue, Maps.newHashMap(), admin, 1L);
    }

    @Test
    public void testDontRestrict() throws Exception
    {
        final IssueEventUserPredicate predicate = getPredicate(EasyMap.build(
                IssueEventTrigger.RESTRICT_EVENT_AUTHORS, newArrayList("false")));

        assertTrue(predicate.apply(adminEvent));
        assertTrue(predicate.apply(fredEvent));
    }

    @Test
    public void testNoOptions() throws Exception
    {
        final IssueEventUserPredicate predicate = getPredicate(EasyMap.build(
                IssueEventTrigger.RESTRICT_EVENT_AUTHORS, newArrayList("true")));

        assertTrue(predicate.apply(adminEvent));
        assertTrue(predicate.apply(fredEvent));
    }

    @Test
    public void testRestrictReporter() throws Exception
    {
        final IssueEventUserPredicate predicate = getPredicate(EasyMap.build(
                IssueEventTrigger.RESTRICT_EVENT_AUTHORS, newArrayList("true"),
                "currentIsReporter", newArrayList("true")));

        when(issue.getReporter()).thenReturn(admin);
        assertTrue(predicate.apply(adminEvent));
        assertFalse(predicate.apply(fredEvent));
    }

    @Test
    public void testRestrictNotReporter() throws Exception
    {
        final IssueEventUserPredicate predicate = getPredicate(EasyMap.build(
                IssueEventTrigger.RESTRICT_EVENT_AUTHORS, newArrayList("true"),
                "enabledReporterRestrictions", newArrayList("true"),
                "currentIsReporter", newArrayList("false")));

        when(issue.getReporter()).thenReturn(admin);
        assertFalse(predicate.apply(adminEvent));
        assertTrue(predicate.apply(fredEvent));
    }

    @Test
    public void testRestrictAssignee() throws Exception
    {
        final IssueEventUserPredicate predicate = getPredicate(EasyMap.build(
                IssueEventTrigger.RESTRICT_EVENT_AUTHORS, newArrayList("true"),
                "currentIsAssignee", newArrayList("true")));

        when(issue.getAssignee()).thenReturn(admin);
        assertTrue(predicate.apply(adminEvent));
        assertFalse(predicate.apply(fredEvent));
    }

    @Test
    public void testRestrictNotAssignee() throws Exception
    {
        final IssueEventUserPredicate predicate = getPredicate(EasyMap.build(
                IssueEventTrigger.RESTRICT_EVENT_AUTHORS, newArrayList("true"),
                "enabledAssigneeRestrictions", newArrayList("true"),
                "currentIsAssignee", newArrayList("false")));

        when(issue.getAssignee()).thenReturn(admin);
        assertFalse(predicate.apply(adminEvent));
        assertTrue(predicate.apply(fredEvent));
    }

    @Test
    public void testRestrictCFNoValues() throws Exception
    {
        final IssueEventUserPredicate predicate = getPredicate(EasyMap.build(
                IssueEventTrigger.RESTRICT_EVENT_AUTHORS, newArrayList("true"),
                IssueEventTrigger.USER_CF_ID, newArrayList("customfield_10000")));

        final CustomField mockCF = Mockito.mock(CustomField.class);
        when(mockCF.getCustomFieldType()).thenReturn(Mockito.mock(UserCFType.class));
        when(customFieldManager.getCustomFieldObject("customfield_10000")).thenReturn(mockCF);

        when(issue.getCustomFieldValue(mockCF)).thenReturn(Collections.emptyList());
        assertFalse(predicate.apply(adminEvent));
        assertFalse(predicate.apply(fredEvent));
    }

    @Test
    public void testRestrictCF() throws Exception
    {
        final IssueEventUserPredicate predicate = getPredicate(EasyMap.build(
                IssueEventTrigger.RESTRICT_EVENT_AUTHORS, newArrayList("true"),
                IssueEventTrigger.USER_CF_ID, newArrayList("customfield_10000")));

        final CustomField mockCF = Mockito.mock(CustomField.class);
        when(mockCF.getCustomFieldType()).thenReturn(Mockito.mock(UserCFType.class));
        when(customFieldManager.getCustomFieldObject("customfield_10000")).thenReturn(mockCF);

        when(issue.getCustomFieldValue(mockCF)).thenReturn(newArrayList(applicationFred));
        assertFalse(predicate.apply(adminEvent));
        assertTrue(predicate.apply(fredEvent));
    }

    @Test
    public void testRestrictSpecificUser() throws Exception
    {
        final IssueEventUserPredicate predicate = getPredicate(EasyMap.build(
                IssueEventTrigger.RESTRICT_EVENT_AUTHORS, newArrayList("true"),
                IssueEventTrigger.SPECIFIC_USER, newArrayList("fred")));

        assertFalse(predicate.apply(adminEvent));
        assertTrue(predicate.apply(fredEvent));
    }

    @Test
    public void testRestrictSpecificGroup() throws Exception
    {
        final IssueEventUserPredicate predicate = getPredicate(EasyMap.build(
                IssueEventTrigger.RESTRICT_EVENT_AUTHORS, newArrayList("true"),
                IssueEventTrigger.IN_GROUP, newArrayList("true"),
                IssueEventTrigger.SPECIFIC_GROUP, newArrayList("admins")));

        assertTrue(predicate.apply(adminEvent));
        assertFalse(predicate.apply(fredEvent));
    }

    @Test
    public void testRestrictNotSpecificGroup() throws Exception
    {
        final IssueEventUserPredicate predicate = getPredicate(EasyMap.build(
                IssueEventTrigger.RESTRICT_EVENT_AUTHORS, newArrayList("true"),
                IssueEventTrigger.IN_GROUP, newArrayList("false"),
                IssueEventTrigger.SPECIFIC_GROUP, newArrayList("admins")));

        assertFalse(predicate.apply(adminEvent));
        assertTrue(predicate.apply(fredEvent));
    }

    private IssueEventUserPredicate getPredicate(final Map params)
    {
        @SuppressWarnings ("unchecked")
        final DefaultAutomationConfiguration config = new DefaultAutomationConfiguration(0, null, params);
        return new IssueEventUserPredicate(config, customFieldManager, groupManager)
        {

        };
    }
}
