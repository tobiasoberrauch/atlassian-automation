package it.com.atlassian.plugin.automation.jira.conditions;

import com.atlassian.jira.testkit.client.IssuesControl;
import com.atlassian.pageobjects.elements.query.AbstractTimedCondition;

public class IssueFieldChangedCondition extends AbstractTimedCondition
{

    private static final int MAX_TIMEOUT = 25000;
    private static final int POLLING_INTERVAL = 500;

    private final IssuesControl issuesControl;
    private final String issueKey;
    private final Object expectedValue;
    private final String fieldName;


    public IssueFieldChangedCondition(final IssuesControl issuesControl, String issueKey, String fieldName, Object expectedValue)
    {
        super(MAX_TIMEOUT, POLLING_INTERVAL);
        this.issuesControl = issuesControl;
        this.issueKey = issueKey;
        this.fieldName = fieldName;
        this.expectedValue = expectedValue;
    }

    @Override
    protected Boolean currentValue()
    {
        final Object currentValue = issuesControl.getIssue(issueKey).fields.get(fieldName);

        if (currentValue == null && expectedValue != null)
        {
            return false;
        }
        else if (currentValue == null)
        {
            return true;
        }

        return currentValue.equals(expectedValue);
    }



}
