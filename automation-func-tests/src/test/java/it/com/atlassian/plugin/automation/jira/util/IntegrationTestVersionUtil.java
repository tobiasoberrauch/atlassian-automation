package it.com.atlassian.plugin.automation.jira.util;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.InputStream;

/**
 * @author: mkonecny
 */
public class IntegrationTestVersionUtil
{
    public static final int JIRA_62_BUILD_NUMBER = 6252;

    public static int getJiraBuildNumber(String baseUrl) throws Exception
    {
        final HttpGet request = new HttpGet(baseUrl + "/rest/api/latest/serverInfo");
        final HttpClient httpClient = new DefaultHttpClient();
        final HttpResponse response = httpClient.execute(request);
        final InputStream input = response.getEntity().getContent();
        return new JSONObject(IOUtils.toString(input)).getInt("buildNumber");
    }
}
