package com.atlassian.plugin.automation.page.action;

import com.atlassian.jira.pageobjects.components.fields.MultiSelect;
import com.atlassian.pageobjects.elements.CheckboxElement;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.plugin.automation.page.ModuleKey;
import org.openqa.selenium.By;

import java.util.Set;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

@ModuleKey ("com.atlassian.plugin.automation.jira-automation-plugin:edit-labels-action")
public class EditLabelsActionForm extends ActionForm
{
    public EditLabelsActionForm(final PageElement container, final String moduleKey)
    {
        super(container, moduleKey);
    }

    public EditLabelsActionForm comment(final String comment)
    {
        setActionParam("jiraComment", comment);
        return this;
    }

    public EditLabelsActionForm dispatchEvent(final boolean shouldDispatch)
    {

        final CheckboxElement checkbox = container.find(By.name("jiraEditLabelsNotification"), CheckboxElement.class);
        if (shouldDispatch)
        {
            checkbox.check();
        }
        else
        {
            checkbox.uncheck();
        }
        return this;
    }

    public EditLabelsActionForm labelsToRemove(Set<String> labels)
    {
        return setLabelMultiSelect(labels, "jiraEditLabelsRemoveField");
    }

    public EditLabelsActionForm labelsToAdd(Set<String> labels)
    {
        return setLabelMultiSelect(labels, "jiraEditLabelsAddField");
    }

    private EditLabelsActionForm setLabelMultiSelect(Set<String> labels, String fieldName)
    {
        final PageElement element = container.find(By.name(fieldName));
        waitUntilTrue(element.timed().isPresent());
        final MultiSelect removeMultiSelect = pageBinder.bind(MultiSelect.class, fieldName);
        removeMultiSelect.clearAllItems();
        for (String label : labels)
        {
            removeMultiSelect.add(label);
        }
        return this;
    }
}
