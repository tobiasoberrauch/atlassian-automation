package com.atlassian.plugin.automation.page.util;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import org.openqa.selenium.By;

import javax.annotation.Nullable;

public class ValidationUtils
{
    public static Iterable<String> getAllErrors(PageElementFinder elementFinder)
    {
        final Iterable<String> errors = Iterables.transform(elementFinder.findAll(By.className("error"), TimeoutType.AJAX_ACTION), new Function<PageElement, String>()
        {
            @Override
            public String apply(@Nullable PageElement input)
            {
                return input.getText();
            }
        });
        return errors;
    }
}
