package com.atlassian.plugin.automation.page;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElement;

import javax.inject.Inject;

public class FormFactory
{
    @Inject
    private PageBinder pageBinder;

    public <T> T build(Class<T> clazz, final PageElement childForm)
    {
        final ModuleKey annotation = clazz.getAnnotation(ModuleKey.class);
        if (annotation == null)
        {
            throw new IllegalArgumentException("Class needs to provide proper annotation");
        }
        final String moduleKey = annotation.value();
        return pageBinder.bind(clazz, childForm, moduleKey);
    }
}
