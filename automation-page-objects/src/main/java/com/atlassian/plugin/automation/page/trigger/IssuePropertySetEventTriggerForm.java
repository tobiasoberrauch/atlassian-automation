package com.atlassian.plugin.automation.page.trigger;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.plugin.automation.page.ModuleKey;
import com.google.common.base.Joiner;

/**
 * Represents an issue property set event trigger.
 */
@ModuleKey("com.atlassian.plugin.automation.jira-automation-plugin:issue-property-set-event-trigger")
public class IssuePropertySetEventTriggerForm extends TriggerForm
{
    /**
     * Module key might be null if no trigger should be set
     */
    public IssuePropertySetEventTriggerForm(final PageElement container, final String moduleKey)
    {
        super(container, moduleKey);
    }

    public IssuePropertySetEventTriggerForm setEventPropertyKeys(String... eventPropertyKeys)
    {
        setTriggerParam("eventPropertyKeys", Joiner.on(",").join(eventPropertyKeys));
        return this;
    }

    public IssuePropertySetEventTriggerForm setJql(String jql)
    {
        setTriggerParam("jiraJqlExpression", jql);
        return this;
    }

}
