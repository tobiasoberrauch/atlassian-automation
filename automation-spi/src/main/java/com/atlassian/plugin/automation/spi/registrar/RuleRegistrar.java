package com.atlassian.plugin.automation.spi.registrar;

import com.atlassian.sal.api.scheduling.PluginJob;

import java.util.Map;

/**
 * This product-specific implementation should make it possible to register/unregister different rules in the host products.
 * Sometimes, extra work is needed when registering/unregistering a rule, so this enables each product to handle this in
 * some specific way.
 */
public interface RuleRegistrar
{
    /**
     * Registers rule which is event triggered, allowing specific product implementations do this e.g. cluster wide
     * @param ruleId
     * @param eventHandler
     * @param notify
     */
    void registerEventTriggeredRule(int ruleId, Object eventHandler, boolean notify);

    /**
     * Registers rule which is CRON triggered, allowing specific product implementations do this e.g. cluster wide
     * @param ruleId
     * @param jobParams
     * @param pluginJobClass needed for proper class loader
     * @param notify
     */
    void registerCronTriggeredRule(int ruleId, Map<String, Object> jobParams, Class<? extends PluginJob> pluginJobClass, boolean notify);

    /**
     * Unregisters given rule. If it doesn't exist, this method does nothing
     *
     * @param ruleId
     * @param notify
     */
    void unregisterRule(int ruleId, boolean notify);

    /**
     * Unregisters all rules
     */
    void unregisterAllRules();

    /**
     * Checks if the rule is registered
     *
     * @param ruleId
     * @return true if the given rule is registered
     */
    boolean isRuleRegistered(int ruleId);

    /**
     * Register handler for changes in the rules configuration
     *
     * @param handler
     */
    void registerRuleChangedHandler(RuleModificationListener handler);

    /**
     * Unregister handler for changes in the rules configuration
     *
     * @param handler
     */
    void unregisterRuleChangedHandler(RuleModificationListener handler);
}
