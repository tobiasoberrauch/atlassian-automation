package com.atlassian.plugin.automation.spi.build;

public interface BuildNumberService
{
    /**
     * @return Build number of the application
     */
    long getBuildNumber();
}
