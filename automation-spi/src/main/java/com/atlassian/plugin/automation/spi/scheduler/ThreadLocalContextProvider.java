package com.atlassian.plugin.automation.spi.scheduler;

/**
 * SPI for setup/teardown of running a job on different thread in products
 */
public interface ThreadLocalContextProvider
{
    /**
     * Provides setup of local context for running jobs in separate thread
     */
    void preCall();

    /**
     * Provides teardown of local context after running jobs in separate thread
     */
    void postCall();
}
