package com.atlassian.plugin.automation.confluence.spi;

import com.atlassian.plugin.automation.spi.scheduler.CronScheduler;
import com.atlassian.sal.api.component.ComponentLocator;
import com.atlassian.sal.api.scheduling.PluginJob;
import org.apache.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Map;

import static java.lang.String.format;
import static org.quartz.Scheduler.DEFAULT_GROUP;

/**
 */
public class ConfluenceCronScheduler implements CronScheduler
{
    private static final Logger log = Logger.getLogger(ConfluenceCronScheduler.class);

    @Override
    public synchronized void scheduleJob(final String jobKey, final Map<String, Object> jobDataMap, final Class<? extends PluginJob> jobClass)
    {
        final Scheduler scheduler = ComponentLocator.getComponent(Scheduler.class);
        final String cronExpression = jobDataMap.get(CronScheduler.JOB_KEY_CRON_EXPRESSION).toString();

        try
        {
            final Trigger cronTrigger = new CronTrigger(jobKey, DEFAULT_GROUP, cronExpression);
            final JobDetail jobDetail = new JobDetail(jobKey, DEFAULT_GROUP, QuartzPluginJob.class, false, true, false);

            if (Arrays.asList(scheduler.getJobNames(DEFAULT_GROUP)).contains(jobKey))
            {
                log.info(String.format("Job with name '%s' already scheduled. Unscheduling first...", jobKey));
                unscheduleJob(jobKey);
            }

            final JobDataMap jobDetailMap = new JobDataMap();
            jobDetailMap.put(QuartzPluginJob.JOB_CLASS_KEY, jobClass);
            jobDetailMap.put(QuartzPluginJob.JOB_DATA_MAP_KEY, jobDataMap);
            jobDetail.setJobDataMap(jobDetailMap);

            scheduler.scheduleJob(jobDetail, cronTrigger);
        }
        catch (ParseException e)
        {
            log.error(format("Error scheduling job '%s' due to invalid cronExpression '%s'.", jobKey, cronExpression), e);
        }
        catch (SchedulerException e)
        {
            log.error(format("Error scheduling job '%s'.", jobKey), e);
        }

    }

    @Override
    public synchronized void unscheduleJob(final String jobKey)
    {
        try
        {
            final Scheduler scheduler = ComponentLocator.getComponent(Scheduler.class);
            if (Arrays.asList(scheduler.getJobNames(DEFAULT_GROUP)).contains(jobKey))
            {
                scheduler.unscheduleJob(jobKey, DEFAULT_GROUP);
                scheduler.deleteJob(jobKey, DEFAULT_GROUP);
            }
        }
        catch (final SchedulerException e)
        {
            throw new IllegalArgumentException(format("Error unscheduling job '%s'.", jobKey), e);
        }
    }
}
