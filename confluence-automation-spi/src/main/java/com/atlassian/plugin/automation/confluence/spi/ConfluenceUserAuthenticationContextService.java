package com.atlassian.plugin.automation.confluence.spi;

import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.plugin.automation.spi.scheduler.UserAuthenticationContextService;

import static com.google.common.base.Strings.isNullOrEmpty;

public class ConfluenceUserAuthenticationContextService implements UserAuthenticationContextService
{
    private final UserAccessor userAccessor;

    public ConfluenceUserAuthenticationContextService(final UserAccessor userAccessor)
    {
        this.userAccessor = userAccessor;
    }

    @Override
    public void setCurrentUser(String username)
    {
        ConfluenceUser user = null;
        if (!isNullOrEmpty(username))
        {
            user = userAccessor.getUserByName(username);
        }

        AuthenticatedUserThreadLocal.set(user);
    }

    @Override
    public String getCurrentUser()
    {
        // The UserAccessor does not provide the current user, so just return null for now
        return null;
    }
}
