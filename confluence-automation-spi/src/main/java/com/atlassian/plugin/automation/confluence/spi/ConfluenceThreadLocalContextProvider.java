package com.atlassian.plugin.automation.confluence.spi;

import com.atlassian.plugin.automation.spi.scheduler.ThreadLocalContextProvider;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;

import javax.inject.Named;

/**
 * Local thread context provider for Confluence
 */
@Named
@ExportAsService
public class ConfluenceThreadLocalContextProvider implements ThreadLocalContextProvider
{
    @Override
    public void preCall()
    {
        // do nothing
    }

    @Override
    public void postCall()
    {
        // do nothing
    }
}
