package com.atlassian.plugin.automation.jira.spi.scheduler;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.plugin.automation.spi.scheduler.CronScheduler;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.scheduling.PluginJob;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.RunMode;
import com.atlassian.scheduler.config.Schedule;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;

import javax.inject.Named;
import java.io.Serializable;
import java.util.Map;
import java.util.Set;

/**
 * Contains the scheduler for the new atlassian-scheduler API
 */
@Named
@ExportAsService
public class JiraCronScheduler implements CronScheduler, DisposableBean
{
    private static final Logger log = Logger.getLogger(JiraCronScheduler.class);
    public static final String JIRA_CRON_SCHEDULER_JOB_RUNNER = "JiraCronSchedulerJobRunner";

    private final Set<JobRunnerKey> jobRunnerKeys = Sets.newHashSet();

    @Override
    public synchronized void scheduleJob(final String jobKey, final Map<String, Object> jobDataMap, final Class<? extends PluginJob> jobClass)
    {
        final SchedulerService scheduler = ComponentAccessor.getComponentOfType(SchedulerService.class);
        final JobRunnerKey jobRunnerKey = JobRunnerKey.of(String.format("%s.%s", JIRA_CRON_SCHEDULER_JOB_RUNNER, jobKey));
        unscheduleJob(jobKey);
        scheduler.unregisterJobRunner(jobRunnerKey);
        scheduler.registerJobRunner(jobRunnerKey, new JiraJobRunnerWrapper(jobDataMap, jobClass.getClassLoader()));
        final Schedule schedule = Schedule.forCronExpression(jobDataMap.get(CronScheduler.JOB_KEY_CRON_EXPRESSION).toString());

        final JobConfig jobConfig = JobConfig.forJobRunnerKey(jobRunnerKey)
                .withRunMode(RunMode.RUN_ONCE_PER_CLUSTER)
                .withParameters(ImmutableMap.<String, Serializable>of(
                                JiraJobRunnerWrapper.JOB_CLASS_CANONICAL_NAME, jobClass.getCanonicalName()))
                .withSchedule(schedule);
        try
        {
            scheduler.scheduleJob(JobId.of(jobKey), jobConfig);
            jobRunnerKeys.add(jobRunnerKey);
        }
        catch (SchedulerServiceException e)
        {
            log.error(String.format("Error scheduling job '%s'.", jobKey), e);

            // as part of cleanup we should try to unregister the job runner
            try
            {
                scheduler.unregisterJobRunner(jobRunnerKey);
            }
            catch (Exception ex)
            {
                log.error("Unable to unregister the JobRunner with key " + jobRunnerKey, ex);
            }
        }
    }

    @Override
    public synchronized void unscheduleJob(final String jobKey)
    {
        final SchedulerService scheduler = ComponentAccessor.getComponentOfType(SchedulerService.class);
        scheduler.unscheduleJob(JobId.of(jobKey));
    }

    @Override
    public void destroy()
    {
        // cleanup of our registered job runners
        final SchedulerService scheduler = ComponentAccessor.getComponentOfType(SchedulerService.class);
        for (JobRunnerKey jobRunnerKey : jobRunnerKeys)
        {
            scheduler.unregisterJobRunner(jobRunnerKey);
        }
    }
}
