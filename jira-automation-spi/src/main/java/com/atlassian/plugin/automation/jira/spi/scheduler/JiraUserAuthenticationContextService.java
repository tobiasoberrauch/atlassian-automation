package com.atlassian.plugin.automation.jira.spi.scheduler;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.plugin.automation.spi.scheduler.UserAuthenticationContextService;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import javax.inject.Inject;
import javax.inject.Named;

import static com.google.common.base.Strings.isNullOrEmpty;

@Named
@ExportAsService
public class JiraUserAuthenticationContextService implements UserAuthenticationContextService
{
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final UserUtil userUtil;

    @Inject
    public JiraUserAuthenticationContextService(@ComponentImport final JiraAuthenticationContext jiraAuthenticationContext,
                                                @ComponentImport final UserUtil userUtil)
    {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.userUtil = userUtil;
    }

    @Override
    public void setCurrentUser(String username)
    {
        ApplicationUser user = null;
        if (!isNullOrEmpty(username))
        {
            user = userUtil.getUserByName(username);
        }

        jiraAuthenticationContext.setLoggedInUser(user);
    }

    @Override
    public String getCurrentUser()
    {
        ApplicationUser user = jiraAuthenticationContext.getUser();
        return (user != null) ? user.getName() : null;
    }
}
